import firebase from 'firebase/app';
import 'firebase/storage';

var firebaseConfig = {
    apiKey: "AIzaSyD7XRdZjKHB0HG8S3s1WEKU5-n_7lCjlh8",
    authDomain: "reactquiz-3f599.firebaseapp.com",
    databaseURL: "https://reactquiz-3f599.firebaseio.com",
    projectId: "reactquiz-3f599",
    storageBucket: "reactquiz-3f599.appspot.com",
    messagingSenderId: "765796366723",
    appId: "1:765796366723:web:746ee24ca5c55fcfc8deb6",
    measurementId: "G-R4HV7EFZ7C"
  };

   firebase.initializeApp(firebaseConfig);

const storage = firebase.storage();
export {
  storage, firebase as default
}
