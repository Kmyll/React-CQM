import React from 'react'
import './App.css'
import Email from './components/Email'
import Quiz from './components/Quiz'
import Avatar from './components/Avatar'


class App extends React.Component{


render(){
  return (
    <div className="App">
      <h1 className="title">QCM React</h1>
      <div className="line"></div>
      <Avatar/>
      <Email/>
      <div className="line2"></div>
      <Quiz/>
    </div>
  );
}
}
export default App;
