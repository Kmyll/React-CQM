import React from 'react'


class Email extends React.Component{

  constructor (props) {
    super(props);
    this.state = {
      email: '',
      formErrors: {email: ''},
      emailValid: false,
      formValid: false
    }
  }

  handleUserInput (e) {
  const name = e.target.name;
  const value = e.target.value;
  this.setState({[name]: value});
}

validateField = () => {
  const {email} = this.state
  if(email == ""){
    alert('please fill username')

  }
  return true;

  }


render() {
  return (
    <div className="App">
    <form className="emailForm">
      <label className="emailLabel"> Merci d'entrer votre adresse E-mail </label>
      <input
        className="emailField"
        value='email'
        placeholder='E-mail'
        name='email'
        onChange={(event) => this.handleUserInput(event)}/>

      <button
        className="emailSubmit"
        type='submit'
        onPress={() => this.validateField}
        value={this.state.email}>
        Valider
      </button>
      </form>
    </div>
  );
}
}
export default Email;
