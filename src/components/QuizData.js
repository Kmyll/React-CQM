export const QuizData = [
    {
      id: 0,
      question: `React est-il un framework ?`,
      options: [`Oui`, `Non`],
      answer: `Non`,
      explaination: `React n'est pas un framework car c'est une librairie`,
    },
    {
      id: 0,
      question: `JSX c'est quoi ?`,
      options: [`JSX est un language compilé`, `JSX est un sur-ensemble développé par Facebook`],
      answer: `JSX est un sur-ensemble développé par Facebook`,
      explaination: `JSX est un sur-ensemble développé par Facebook`
    },
    {
      id: 0,
      question: `Sélectionnez ci-dessous dans la liste la bonne définition caractérisant ce framework PHP`,
      options: [`MMV`, `MVC`, `MMVV`],
      answer: `MVC`,
      explaination: `Symfony est basé sur le pattern MVC`
    }
];
