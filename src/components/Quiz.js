import React from 'react'
import {QuizData} from './QuizData'
import Avatar from './Avatar'


class Quiz extends React.Component{
  state = {
    userAnswer: null,
    currentQuestion: 0,
    options: [],
    quizEnd : false,
    score: 0,
    disabled: true
  };

  loadQuiz = () =>{
    const {currentQuestion} = this.state;
    this.setState(() => {
      return{
        questions: QuizData[currentQuestion].question,
        options: QuizData[currentQuestion].options,
        answer: QuizData[currentQuestion].answer,
      };
    });
  }

  componentDidMount(){
    this.loadQuiz();
  }

  nextQuestionHandler = () => {
    const {userAnswer, answer, score} = this.state;
    this.setState({
      currentQuestion: this.state.currentQuestion + 1
    })
    console.log(this.state.currentQuestion)

    // Score incrementation
    if(userAnswer === answer){
      this.setState({
      score: score + 1.5
      })
    }
  }

  // Update MCQ quiz questions
  componentDidUpdate(prevProps, prevState){
    const {currentQuestion} = this.state
    if(this.state.currentQuestion !== prevState.currentQuestion){
      this.setState(() => {
        return{
          disabled:true,
          questions: QuizData[currentQuestion].question,
          options: QuizData[currentQuestion].options,
          answer: QuizData[currentQuestion].answer,
        };
      })
    }
  }
  checkAnswer = answer => {
    this.setState({
      userAnswer: answer,
      disabled: false
    })
  }

  finishHandler = () => {
  if(this.state.currentQuestion === QuizData.length - 1){
    this.setState({
      quizEnd: true
    })
  }
}


render() {
  const {questions, options, currentQuestion, userAnswer, quizEnd} = this.state;
  if(quizEnd) {
    return(
      <div>
        <h2>Félicitations, votre QCM est complété. </h2>
          <p className="score">Votre score est de {this.state.score}/3</p>
          <p>Les résponses correctes sont : </p>
          <ul className="replyList">
              {QuizData.map((item, index) => (
                <li
                className="answersCheck"
                key={index}>
                {item.explaination}
                </li>
              ))}
          </ul>
      </div>
    )
  }

  return (
    <div className='outsideContainer'>
    <div className="main-container">
      <h2><span className="question">Question:</span> {questions}</h2>
      <span className="questionCounter">{`Question ${currentQuestion + 1 } sur ${QuizData.length}`}</span>
      {options.map(option => (
        <p
            key={option.id}
            className={`quizOptions
              ${userAnswer === option ? "selected" : null}
            `}
            onClick={() => this.checkAnswer(option)}
            >
        {option}
        </p>
      ))}
      {currentQuestion < QuizData.length - 1 &&
        <button
            className="submitBtn"
            disabled = {this.state.disabled}
            onClick={this.nextQuestionHandler}>
            Question suivante
        </button>
      }
      {currentQuestion === QuizData.length - 1 &&
      <button
            className="submitBtn"
            disabled = {this.state.disabled}
            onClick = {this.finishHandler}>
            Envoyer
      </button>
      }
    </div>
    </div>
  );
}
}
export default Quiz;
