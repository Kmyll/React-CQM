import React from 'react'
import { storage } from '../config/Firebase'

class Avatar extends React.Component{
  constructor(props){
    super(props);
    this.state = {
      image: null,
      url: '',
      progress: 0
    }
    this.handleChange = this.handleChange.bind(this);
    this.handleUpload = this.handleUpload.bind(this);
  }
  handleChange = e => {
    if(e.target.files[0]){
      const image = e.target.files[0];
      this.setState(() => ({image}));
    }
  }

  handleUpload = () => {
    const {image} = this.state;
    const uploadTask = storage.ref(`images/${image.name}`).put(image);
    uploadTask.on('state_changed',
      (snapshot) => {
        const progress = Math.round((snapshot.bytesTransferred / snapshot.totalBytes) * 100);
        this.setState({progress})
    },
      (error) => {
        console.log(error);
    },
      () => {
        storage.ref('images').child(image.name).getDownloadURL().then(url => {
          console.log(url);
          this.setState({url});
        })
    });
  }


  render(){
    return(
      <div className="imageContainer">
        <div className="uploadImgContainer">
          <input className="uploadImg" type="file" onChange={this.handleChange}/>
          <button className="uploadImgBtn" onClick={this.handleUpload}>Ajouter l'image</button>
        </div>
        <progress className="progressBar" value={this.state.progress} max="100"/>
        <img className="imgRender" src={this.state.url || 'https://via.placeholder.com/150x150'} alt="uploaded avatar"/>
      </div>
    )
  }

}
export default Avatar
