import React from 'react'

export default function EmailInput(props){

  return (
    <input
      type='text'
      value={props.value}
      placeholder={props.placeholder}
      onChange={props.handleChange}
      className="email" />
  )}
